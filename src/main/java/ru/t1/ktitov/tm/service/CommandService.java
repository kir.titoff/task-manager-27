package ru.t1.ktitov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktitov.tm.api.repository.ICommandRepository;
import ru.t1.ktitov.tm.api.service.ICommandService;
import ru.t1.ktitov.tm.command.AbstractCommand;
import ru.t1.ktitov.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.ktitov.tm.exception.system.CommandNotSupportedException;

import java.util.Collection;

public final class CommandService implements ICommandService {

    @NotNull
    private final ICommandRepository commandRepository;


    public CommandService(@NotNull final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public void add(@Nullable final AbstractCommand command) {
        if (command == null) return;
        commandRepository.add(command);
    }

    @NotNull
    @Override
    public AbstractCommand getCommandByArgument(@Nullable final String argument) {
        if (argument == null || argument.isEmpty()) throw new ArgumentNotSupportedException();
        @Nullable AbstractCommand command = commandRepository.getCommandByArgument(argument);
        if (command == null) throw new ArgumentNotSupportedException();
        return command;
    }

    @NotNull
    @Override
    public AbstractCommand getCommandByName(@Nullable final String name) {
        if (name == null || name.isEmpty()) throw new CommandNotSupportedException();
        @Nullable AbstractCommand command = commandRepository.getCommandByName(name);
        if (command == null) throw new CommandNotSupportedException();
        return command;
    }

    @NotNull
    @Override
    public Collection<AbstractCommand> getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

}

package ru.t1.ktitov.tm.exception.system;

import org.jetbrains.annotations.NotNull;

public final class CommandNotSupportedException extends AbstractSystemException {

    public CommandNotSupportedException() {
        super("Error! Not supported command.");
    }

    public CommandNotSupportedException(@NotNull final String command) {
        super("Error! Not supported command: " + command + ".");
    }

}
